// stack.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

using namespace std;

class StackArr
{
private:
	int const size = 3;
	int* arr;
	int TopID;
	int arr_capacity;
public:
	StackArr()
	{
		arr = new int[size];
		TopID = 0;
		arr_capacity = size;
	}

	~StackArr()
	{
		delete[] arr;
	}

	void Push(int element)
	{
		if (TopID == arr_capacity) // ���� ������
		{
			resize_arr(2);
		}
		arr[TopID] = element;
		TopID++;
	}

	int Pop()
	{
		if (Is_Empty()) // �������� ����
		{
			cout << "Error, No elements in Stack";
			cout << endl;
			return -1;
		}
		else
		{
			if (TopID < (arr_capacity / 3)) //������ ������� ����
			{
				resize_arr(0.5);
			}
			--TopID;
			int result;
			result = arr[TopID];
			return result;
		}

	}

	void resize_arr(double coef)
	{
		int* temporary_arr = new int[arr_capacity];
		for (int i = 0; i < arr_capacity; i++)
		{
			temporary_arr[i] = arr[i];
		}
		delete[] arr;
		int new_size;
		new_size = arr_capacity * coef;
		arr = new int[new_size];
		for (int i = 0; i < TopID; i++)
		{
			arr[i] = temporary_arr[i];
		}
		arr_capacity = new_size;
		delete[] temporary_arr;
	}

	bool Is_Empty()
	{
		return TopID <= 0;
	}
};

bool Test_Stack1() //�������� �� ����������
{
	bool testResult;
	StackArr st;
	testResult = st.Is_Empty();
	return testResult;
}
bool Test_Stack2() //1 �������, �������� �� ����������
{
	bool testResult;
	StackArr st;
	st.Push(20);
	testResult = !st.Is_Empty();
	return testResult;
}
bool Test_Stack3() //�������� �� ����������� ������, push/pop
{
	bool testResult;
	StackArr st;
	st.Push(1);
	st.Push(2);
	st.Push(40);
	testResult = st.Pop() == 41;
	/*testResult &= st.Pop() == 2;
	testResult &= st.Pop() == 1;*/
	return testResult;
}
bool Test_Stack4() //��������� ��������
{
	bool testResult;
	StackArr st;
	for (int i = 1; i <= 100; i++)
	{
		st.Push(i);
	}
	testResult = !st.Is_Empty();
	for (int i = 100; i > 0; i--)
	{
		testResult &= st.Pop() == i;
	}
	testResult &= st.Is_Empty();
	return testResult;
}

int main()
{
	cout << "Test1: Empty = " << boolalpha << Test_Stack1() << endl;
	cout << "Test2: Adding 1 item = " << boolalpha << Test_Stack2() << endl;
	cout << "Test3: Push/Pop check = " << boolalpha << Test_Stack3() << endl;
	cout << "Test4: Global 100 = " << boolalpha << Test_Stack4() << endl;
	return 0;
}